package model;

import java.awt.*;

public class Line extends Shape {
    private Vector vector;

    public Line(World world, Location location, Color backgroundColor, Vector vector) {
        super(world, location, backgroundColor);
        type = ShapeType.LINE;
        this.vector = vector;
    }

    public Vector getVector() {
        return vector;
    }

    public void setVector(Vector vector) {
        this.vector = vector;
    }
}
