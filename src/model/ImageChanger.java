package model;

public class ImageChanger extends Animation {
    private Image image;

    public ImageChanger(Shape shape) {
        super(shape);
        if (shape.getType() != ShapeType.IMAGE)
            throw new IllegalArgumentException("The shape with id #" + shape.getId() + " isn't an Image.");
        image = (Image) shape;
    }

    @Override
    void update(double ratio) {

    }
}
