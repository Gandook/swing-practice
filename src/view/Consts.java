package view;

public enum Consts {
    MAIN_WINDOW_WIDTH(800), MAIN_WINDOW_HEIGHT(600), WINDOW_BAR_HEIGHT(30);

    private int value;

    public int getValue() {
        return value;
    }

    Consts(int value) {
        this.value = value;
    }
}
