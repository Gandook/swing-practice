package model;

public enum ShapeType {
    CIRCLE, LINE, RECTANGLE, IMAGE
}
