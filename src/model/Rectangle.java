package model;

import java.awt.*;

public class Rectangle extends Shape {
    private int height, width;

    public Rectangle(World world, Location location, Color backgroundColor, int height, int width) {
        super(world, location, backgroundColor);
        type = ShapeType.RECTANGLE;
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
