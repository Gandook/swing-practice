package model;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Image extends Shape {
    private BufferedImage image;
    private int width, height;

    public Image(World world, Location location, Color backgroundColor, BufferedImage image, int width, int height) {
        super(world, location, backgroundColor);
        type = ShapeType.IMAGE;
        this.image = image;
        this.width = width;
        this.height = height;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}