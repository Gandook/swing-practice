package model;

public enum Consts {
    ANIMATION_INITIAL_DURATION(1000);

    private int value;

    public int getValue() {
        return value;
    }

    Consts(int value) {
        this.value = value;
    }
}
