package view;

import model.World;

import javax.swing.*;
import java.awt.*;

public class MainWindow {
    private static JFrame frame = new JFrame("Simple Paint");
    private static JPanel contentPane = (JPanel) frame.getContentPane(), showPanel, shapesPanel, shapeDetailsPanel, animationsPanel, animationDetailsPanel, bottomPanel, buttonPanel;
    private static boolean initiated = false;
    private static World world;
    private static JButton ssButton, circleButton, imageButton, rectangleButton, lineButton, displacerButton, sizeChangerButton, imageChangerButton;

    public static void setWorld(World world) {
        MainWindow.world = world;
    }

    private static void initialize() {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        contentPane.setSize(Consts.MAIN_WINDOW_WIDTH.getValue(), Consts.MAIN_WINDOW_HEIGHT.getValue());
        frame.setSize(Consts.MAIN_WINDOW_WIDTH.getValue(), Consts.MAIN_WINDOW_HEIGHT.getValue() + Consts.WINDOW_BAR_HEIGHT.getValue());
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        contentPane.setLayout(new BorderLayout());

        showPanel = new JPanel(null);
        showPanel.setPreferredSize(new Dimension(640, 480));
        showPanel.setBackground(Color.WHITE);
        contentPane.add(showPanel, BorderLayout.CENTER);

        shapesPanel = new JPanel();
        shapesPanel.setLayout(new BoxLayout(shapesPanel, BoxLayout.Y_AXIS));
        shapesPanel.setPreferredSize(new Dimension(80, 480));
        contentPane.add(shapesPanel, BorderLayout.EAST);

        animationsPanel = new JPanel();
        animationsPanel.setLayout(new BoxLayout(animationsPanel, BoxLayout.Y_AXIS));
        animationsPanel.setPreferredSize(new Dimension(80, 480));
        contentPane.add(animationsPanel, BorderLayout.WEST);

        bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
        bottomPanel.setPreferredSize(new Dimension(800, 120));
        contentPane.add(bottomPanel, BorderLayout.SOUTH);

        shapeDetailsPanel = new JPanel(null);
        shapeDetailsPanel.setPreferredSize(new Dimension(272, 120));
        shapeDetailsPanel.setBackground(new Color(109, 135, 185));
        bottomPanel.add(shapeDetailsPanel);

        animationDetailsPanel = new JPanel(null);
        animationDetailsPanel.setPreferredSize(new Dimension(408, 120));
        animationDetailsPanel.setBackground(new Color(141, 106, 150));
        bottomPanel.add(animationDetailsPanel);

        buttonPanel = new JPanel(new BorderLayout());
        buttonPanel.setPreferredSize(new Dimension(120, 120));
        bottomPanel.add(buttonPanel);

        ssButton = new JButton("Start / Stop");
        ssButton.setPreferredSize(new Dimension(120, 120));
        buttonPanel.add(ssButton, BorderLayout.CENTER);

        circleButton = new JButton("Circle");
        circleButton.setPreferredSize(new Dimension(80, 120));
        shapesPanel.add(circleButton);

        rectangleButton = new JButton("Rectangle");
        rectangleButton.setPreferredSize(new Dimension(80, 120));
        shapesPanel.add(rectangleButton);

        lineButton = new JButton("Line");
        lineButton.setPreferredSize(new Dimension(80, 120));
        shapesPanel.add(lineButton);

        imageButton = new JButton("Image");
        imageButton.setPreferredSize(new Dimension(80, 120));
        shapesPanel.add(imageButton);

        displacerButton = new JButton("Displacer");
        displacerButton.setPreferredSize(new Dimension(80, 160));
        animationsPanel.add(displacerButton);

        sizeChangerButton = new JButton("Size Changer");
        sizeChangerButton.setPreferredSize(new Dimension(80, 160));
        animationsPanel.add(sizeChangerButton);

        imageChangerButton = new JButton("Image Changer");
        imageChangerButton.setPreferredSize(new Dimension(80, 160));
        animationsPanel.add(imageChangerButton);
    }

    public static void show() {
        if (!initiated) {
            initialize();
            initiated = true;
        }
        frame.setVisible(true);
    }
}
