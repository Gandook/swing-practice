package model;

public class SizeChanger extends Animation {
    private double changeRatio;

    public SizeChanger(Shape shape, double changeRatio) {
        super(shape);
        this.changeRatio = changeRatio;
    }

    @Override
    void update(double ratio) {
        ShapeType type = SHAPE.getType();

        switch (type) {
            case LINE:
                Line line = (Line) SHAPE;
                Vector vector = line.getVector();
                line.setVector(new Vector((int) ((double) vector.getDx() + ratio * (changeRatio - 1) * vector.getDx()), (int) ((double) vector.getDy() + ratio * (changeRatio - 1) * vector.getDy())));
                break;
            case IMAGE:
                Image image = (Image) SHAPE;
                int imageWidth = image.getWidth(), imageHeight = image.getHeight();
                image.setWidth((int) ((double) imageWidth + ratio * (changeRatio - 1) * imageWidth));
                image.setHeight((int) ((double) imageHeight + ratio * (changeRatio - 1) * imageHeight));
                break;
            case CIRCLE:
                Circle circle = (Circle) SHAPE;
                int radius = circle.getRadius();
                circle.setRadius((int) ((double) radius + ratio * (changeRatio - 1) * radius));
                break;
            case RECTANGLE:
                Rectangle rectangle = (Rectangle) SHAPE;
                int rectangleWidth = rectangle.getWidth(), rectangleHeight = rectangle.getHeight();
                rectangle.setWidth((int) ((double) rectangleWidth + ratio * (changeRatio - 1) * rectangleWidth));
                rectangle.setHeight((int) ((double) rectangleHeight + ratio * (changeRatio - 1) * rectangleHeight));
                break;
        }
    }
}
