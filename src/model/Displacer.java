package model;

public class Displacer extends Animation {
    private Vector vector;

    public Displacer(Shape shape, Vector vector) {
        super(shape);
        this.vector = vector;
    }

    @Override
    void update(double ratio) {
        Location curLocation = SHAPE.getLocation();
        SHAPE.setLocation(new Location((int) ((double) curLocation.getX() + ratio * vector.getDx()), (int) ((double) curLocation.getY() + ratio * vector.getDy())));
    }
}
