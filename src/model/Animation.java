package model;

import java.util.Objects;

public abstract class Animation extends Thread {
    final Shape SHAPE;
    private long id, duration, startTime;
    private final long WAIT_INTERVAL_MILLIS = 50;
    private boolean repeating, active, running;

    Animation(Shape shape) {
        SHAPE = shape;
        id = shape.getAnimationId();
        this.duration = Consts.ANIMATION_INITIAL_DURATION.getValue();
        repeating = false;
        active = false;
        running = false;
    }

    public void setRepeating(boolean repeating) {
        this.repeating = repeating;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public long getId() {
        return id;
    }

    abstract void update(double ratio);

    @Override
    public void run() {
        try {
            long curTime;
            running = true;
            while (running) {
                curTime = System.currentTimeMillis();
                if (curTime - startTime >= duration * 2) {
                    if (repeating)
                        startTime = System.currentTimeMillis();
                    else
                        running = false;
                } else {
                    synchronized (SHAPE) {
                        update((double) 1 - Math.abs((double) curTime - (startTime + duration)) / duration);
                    }
                }
                sleep(WAIT_INTERVAL_MILLIS);
            }
            update(0);
        } catch (InterruptedException e) {
            System.out.printf("The animation with id #%lld was interrupted.\n", id);
        }
    }

    @Override
    public synchronized void start() {
        startTime = System.currentTimeMillis();
        run();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Animation)) return false;
        Animation animation = (Animation) o;
        return id == animation.id &&
                Objects.equals(SHAPE, animation.SHAPE);
    }
}
