package model;

import java.util.ArrayList;
import java.util.List;

public class World {
    private List<Shape> shapes;
    private long shapeId;

    public World() {
        shapes = new ArrayList<>();
        shapeId = 0;
    }

    public List<Shape> getShapes() {
        return shapes;
    }

    public long getShapeId() {
        return shapeId;
    }

    private boolean findShape(Shape shape) {
        for (Shape curShape : shapes) {
            if (curShape.equals(shape))
                return true;
        }
        return false;
    }

    public synchronized void addShape(Shape shape) throws IllegalStateException {
        if (findShape(shape))
            throw new IllegalStateException("The world already has the shape with id #" + shape.getId());
        shapes.add(shape);
        shapeId++;
    }

    public synchronized void removeShape(Shape shape) throws IllegalStateException {
        if (!findShape(shape))
            throw new IllegalStateException("The world doesn't have the shape with id #" + shape.getId());
        shapes.remove(shape);
    }
}
