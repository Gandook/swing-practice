package model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Shape {
    private Location location;
    private Color backgroundColor;
    private long id, animationId;
    private List<Animation> animations;
    private World world;
    ShapeType type;

    Shape(World world, Location location, Color backgroundColor) {
        this.world = world;
        id = world.getShapeId();
        animationId = 0;
        this.location = location;
        this.backgroundColor = backgroundColor;
        animations = new ArrayList<>();
    }

    public List<Animation> getAnimations() {
        return animations;
    }

    public long getAnimationId() {
        return animationId;
    }

    public ShapeType getType() {
        return type;
    }

    public Location getLocation() {
        return location;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public long getId() {
        return id;
    }

    private boolean findAnimation(Animation animation) {
        for (Animation curAnimation : animations) {
            if (curAnimation.equals(animation))
                return true;
        }
        return false;
    }

    private synchronized void addAnimation(Animation animation) throws IllegalStateException {
        if (findAnimation(animation))
            throw new IllegalStateException("The shape with id #" + id + " already has the animation with id #" + animation.getId());
        animations.add(animation);
        animationId++;
    }

    private synchronized void removeAnimation(Animation animation) throws IllegalStateException {
        if (!findAnimation(animation))
            throw new IllegalStateException("The shape with id #" + id + " doesn't have the animation with id #" + animation.getId());
        animations.remove(animation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shape)) return false;
        Shape shape = (Shape) o;
        return id == shape.id;
    }
}
