package model;

import java.awt.*;

public class Circle extends Shape {
    private int radius;

    public Circle(World world, Location location, Color backgroundColor, int radius) {
        super(world, location, backgroundColor);
        type = ShapeType.CIRCLE;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
